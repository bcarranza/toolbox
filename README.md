# Toolbox

> This is an early iteration. 


## Intentionally Vulnerable aka Vulnerable By Design

> Just as "hello, world" examples are useful, it's often helpful to have a set of things that are intentionally vulnerable for testing purposes. These are the things I have found useful. 

  - [detecting-secrets](https://gitlab.com/bcarranza/detecting-secrets) - A GitLab project based on the work of others. The [**Secret Detection**](https://docs.gitlab.com/ee/user/application_security/secret_detection/) job will find "secrets" and tell you about them. 
  - [Terragoat](https://github.com/bridgecrewio/terragoat/) - TerraGoat is Bridgecrew's "Vulnerable by Design" Terraform repository. TerraGoat is a learning and training project that demonstrates how common configuration errors can find their way into production cloud environments. | [Example Project](https://gitlab.com/bcarranza/iac-scanning-terragoat) running [Infrastructure as Code (IaC) Scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/) on Terragoat. 🐐
  - [ ] _Untested (by me)_ [Sadcloud](https://github.com/nccgroup/sadcloud)

### Lists

  - Cloud Security Wiki has a [list of vulnerable apps](https://cloudsecwiki.com/vulnerable_apps.html). 


## Commands

Generate the info you need when opening an issue:

```
gitlab-rake gitlab:env:info | tee gitlab-env-info.txt &&  gitlab-rake gitlab:check SANITIZE=true | tee gitlab-check-sanitize.txt
```

Make a five minute progress bar


```
for i in $(seq 1 300) ; do echo -n '.' ; sleep 1 ; done
```


Automate a bunch of `git` operations:

```
RANDOM_INFO=$(date +%A-%m-%B-%d_at_%H-%M-%S) &&  LOCAL_BRANCH=$RANDOM_INFO &&  echo $LOCAL_BRANCH && echo "remote branch"  &&    REMOTE_BRANCH=$RANDOM_INFO && echo $REMOTE_BRANCH &&   git checkout -b dev/whatever-$LOCAL_BRANCH\ndate >> package.json\nCOMMIT_MESSAGE=$(curl --silent  http://whatthecommit.com/index.txt)\ngit commit -m"Meow: $COMMIT_MESSAGE" package.json\ngit push -u origin dev/whatever-${LOCAL_BRANCH}:dev/whatever-${REMOTE_BRANCH}   -o merge_request.create   -o merge_request.target=master   -o merge_request.merge_when_pipeline_succeeds  -o ci.variable="SANITY_MODE=TROY_SANITY_SHORT"  -o ci.variable="CUTE_CATS=YESPLEASE "
```

I assembled this for a ticket :ticket: and wanted to hold on to it. 



## Fun

```
gitlab_rails['content_security_policy'] = { enabled: true, report_only: false, directives: { frame_ancestors: "'self'"} }
```

This can cause:

```
Failed to register an agent
 NetworkError when attempting to fetch resource.
```
