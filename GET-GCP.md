# Deploy GitLab to GCP with GET

> GGG.

  - Provision a Cloud Account in GCP with [gitlabsandbox.cloud](https://gitlabsandbox.cloud)
  - Record the **Meta Data**, saving it in `pass`.
  - Click **Create Terraform Environment**. 
  - Choose the **GET** template -- to start, this time.
  - Connect to the GitOps instance. 
  - Find the project and do the workarounds for:
    - [ ] https://gitlab.com/gitlab-com/business-technology/engineering/tools/hackystack/-/issues/92
    - [ ] https://gitlab.com/gitlab-com/business-technology/engineering/tools/hackystack/-/issues/19


## `pass`

```
pass getgcp/cloud-provider
pass getgcp/cloud-realm
pass getgcp/organization-unit
pass getgcp/gcp-project-id
```

```
pass getgcp/iam/cloud-account
```


## :bug: Bugs 

> Bugs I met along the way. 

  - https://gitlab.com/gitlab-org/gitlab/-/issues/338993
